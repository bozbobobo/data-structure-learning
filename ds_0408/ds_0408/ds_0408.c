//
#include "sqlist.h"

int main()
{
	sqlist L = {0};

	ListInit(&L);


	ListInsert(&L, 0, 10);
	ListInsert(&L, 1, 2);
	ListInsert(&L, 2, 2);
	ListInsert(&L, 3, 2); 
	ListInsert(&L, 4, 10);
	
	
	ListShow(&L);
	printf("\n");
	ListDupdel(&L);
	//ListClear(&L);
	//ListDelete(&L, 0);
	printf("************\n");
	ListShow(&L);
	//ListLoc(&L, 5);
	ListFree(&L);
	return 0;
}