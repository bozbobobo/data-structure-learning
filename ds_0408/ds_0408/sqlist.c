#include "sqlist.h"

//1.顺序表创建函数：
/**
***@return:  0:初始化成功   -1:list 初始化失败
***@para:  顺序表的结构体指针，sqlink类型
**/
int ListInit(sqlink list)
{
	//给顺序表中存放数据的data开辟一块长度为初始容量大小的动态内存：malloc
	list->data = (data_t*)malloc(CAP_INIT * sizeof(data_t));
	if (list->data == NULL)
	{
		return -1;
	}

	//data中数据初始化时置空
	memset(list->data, 0, CAP_INIT * sizeof(data_t));
	list->cap = CAP_INIT;//初始化最大容量为初始值
	list->last = -1;//初始化当前位置的下标是-1，说明表中没有元素

	return 0;
}

//2.判断是不是空表：
/**
***@return: -1:表不存在   0：表为空   某一个正数：表中元素个数  
***@para:   顺序表结构体指针
**/
int ListIsEmpty(sqlink list)
{
	//先判断传过来的结构体指针存不存在：
	if (list == NULL)
	{
		return -1;
	}

	else if (-1 == list->last)
	{
		return 0;//如果last是-1，说明刚初始化，是空表
	}

	return list->last + 1;//返回元素个数
}


//3.查表长：
/**
***@return: 表中元素个数
***@para:   顺序表结构体指针
**/
int ListLen(sqlink list)
{
	//先判断传过来的结构体指针存不存在：
	if (list == NULL)
	{
		return -1;
	}

	return list->last + 1;//返回元素个数,last=-1则返回0，说明有0个元素。
}


//4.顺序表插入函数：
/**
***@return: -1：插入失败    0：插入成功
***@para:   list:顺序表结构体指针    pos:想插入的下标   val：要插入的值
**/
int ListInsert(sqlink list, int pos , data_t val)
{
	//先判断表满没满，满了就扩容:realloc
	if (list->cap == (list->last + 1))
	{
		data_t* tmp = NULL;
		tmp	= (data_t*)realloc(list->data, (list->cap + CAP_ADD) * sizeof(data_t));
		if (tmp == NULL)
		{
			printf("扩容失败\n");
			return -1;
		}
		//扩容成功，最大容量增加
		list->data = tmp;
		printf("扩容成功\n");
		list->cap += CAP_ADD;
	}
	
	//判断传过来的参数pos合不合理
	if (pos<0 || pos>(list->last + 2))
	{
		printf("数据插入位置不合理，重新确定参数pos\n");
		return -1;
	}
	
	//移动：从后往前
	for (int i = list->last; i >= pos; i--)
	{
		list->data[i + 1] = list->data[i];
	}

	//插入新元素：
	list->data[pos] = val;

	//更新：
	list->last++;

	return 0;

}

//5.遍历打印函数：
/**
***@return: void
***@para:   顺序表结构体指针
**/
void ListShow(sqlink list)
{
	//先判断传过来的结构体指针存不存在：
	if (list == NULL)
	{
		return;
	}
	//先判断表空不空，空了提示一下
	if (list->last == -1)
	{
		printf("表中无内容\n");
	}

	for (int i = 0; i <= list->last; i++)
	{
		printf("%d ", list->data[i]);
	}
}


//6.顺序表清空函数：
/****
****@return:   void
****@para:     顺序表指针
****/
void ListClear(sqlink list)
{
	memset(list->data, 0, (list->last + 1) * sizeof(data_t));
	list->cap = CAP_INIT;//初始化最大容量为初始值
	list->last = -1;//初始化当前位置的下标是-1，说明表中没有元素
}



//7.动态内存释放函数：
/****
****@return:   void
****@para:     顺序表指针
****/
void ListFree(sqlink list)
{
	if (list == NULL)
	{
		return;
	}

	//将为list->data创建的动态内存释放，之后将指针置空
	free(list->data);
	list->data = NULL;
}

//8.删除顺序表中某元素函数：
/****
****@return:   -1：删除失败   0：删除成功
****@para:     list:顺序表指针   pos:要删除的值的下标
****/
int ListDelete(sqlink list, int pos)
{
	//判断顺序表指针是不是空指针
	if (list == NULL)
	{
		return -1;
	}
	//判断pos合法？
	if (pos<0 || pos>list->last)
	{
		printf("要删除的元素下标不合法\n");
		return - 1;
	}

	//移动：
	for (int i = pos; i < list->last; i++)
	{
		list->data[i] = list->data[i + 1];
	}

	//更新：
	list->last--;

	//判断当前元素个数是不是比容量少太多：少太多就减小开辟的空间，少的不多就保持：
	if ((list->cap - list->last) > 3)
	{
		data_t* sub = NULL;
		sub = (data_t*)realloc(list->data, (list->cap - CAP_ADD) * sizeof(data_t));
		if (sub != NULL)
		{
			list->data = sub;
			list->cap -= CAP_ADD;
		}
		else return -1;
	}

	return 0;

}

//9.查找定位某元素：
/****
****@return:   -1:出错      0：没有这个元素   其他：该元素位置
****@para:     list:顺序表指针   val:要删除的元素的值
****/
int ListLoc(sqlink list, data_t val)
{
	//判断指针是不是空指针：
	if (list == NULL)
	{
		return -1;
	}

	//遍历
	int i = 0;
	for (i = 0; i <= list->last; i++)
	{
		if (val == list->data[i])
		{
			printf("%d的下标是%d\n", val, i);
			return i;
		}
		
	}

	//没找到：i大于最后一个元素的下标就说明没找到
	if (i >= list->last)
	{
		printf("没找到\n");
		return 0;
	}
	return 0;
}

//10.删除重复元素：
int ListDupdel(sqlink list)
{
	//判断空指针？
	if (list == NULL)
	{
		return -1;
	}
	//判断有几个元素，1个或者0个就不执行操作直接退出了
	if (list->last == 0 || list->last == -1)
	{
		return -1;
	}
	//开始删除重复元素：
	int i = 1;
	int j = 0;
	//从第二个元素（下标为1）开始寻找
	//遍历从0到当前元素，有相同的就删除当前元素
	//没有相同的就继续下一个元素开始

	for (i = 1; i <= list->last; i++)
	{
		for (j = 0; j < i; j++)
		{
			if (list->data[i] == list->data[j])
			{
				ListDelete(list, i);
				i--;
				break;
			}
		}
	}
	return 0;
}