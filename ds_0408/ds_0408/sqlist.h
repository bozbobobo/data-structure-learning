#pragma once

//在这个头文件中定义数据结构（一个结构体）、声明顺序表的运算的函数。

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int data_t;//这样一来，data_t的类型就可以直接在这里更改一次就好

#define CAP_ADD 3  //容量满了之后每一次加多少个data_t大小的容量
#define CAP_INIT 3 //初始化容量大小


typedef struct sqlist
{
	data_t* data;//数据内容，可以是一维数组
	int cap;     //当前最大容量
	int last;    //当前数据所在位置下标
}sqlist, * sqlink;

//1.顺序表创建函数：
int ListInit(sqlink list );

//2.判断是不是空表：
int ListIsEmpty(sqlink list);

//3.查表长：
int ListLen(sqlink list);

//4.顺序表插入函数：
int ListInsert(sqlink list, int pos, data_t val);

//5.遍历打印函数：
void ListShow(sqlink list);

//6.顺序表清空函数：
void ListClear(sqlink list);

//7.动态内存释放函数：
void ListFree(sqlink list);

//8.删除顺序表中某元素函数：
int ListDelete(sqlink list, int pos);

//9.查找定位某元素：
int ListLoc(sqlink list, data_t val);

//10.删除重复元素：
int ListDupdel(sqlink list);



