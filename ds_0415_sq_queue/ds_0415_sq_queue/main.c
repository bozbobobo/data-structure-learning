//
//
#include "sequeue.h"

int main()
{
	ptrsq sequeue = queue_init(5);
	int val = 0;
	while (1)
	{
		printf("input:");
		int ret = scanf("%d", &val);
		if (-1 == val)
			break;
		enqueue(sequeue, val);
	}
	queue_show(sequeue);
	dequeue(sequeue);
	dequeue(sequeue);
	dequeue(sequeue);
	enqueue(sequeue, 10);
	enqueue(sequeue, 20);
	queue_show(sequeue);
	queue_clear(sequeue);
	queue_show(sequeue);
	return 0;
}