#pragma once

//本文档主要记录：
//顺序队列的结构定义
//顺序队列的功能实现函数的声明

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int data_t;

typedef struct sequeue
{
	data_t* data;//存放数据
	int front;//队头
	int rear;//队尾
	int maxsize;//队列最大元素个数
}sequeue, * ptrsq;


//1. 创建队列 
ptrsq queue_init(int size);

//2. 入队
int enqueue(ptrsq sq, data_t val);

//3. 出队
data_t dequeue(ptrsq sq);

//4. 队列遍历打印
int queue_show(ptrsq sq);

//5. 清空队列 
int queue_clear(ptrsq sq);

//6. 判断队列空
int queue_empty(ptrsq sq);

//7. 判断队列满
int queue_full(ptrsq sq);

//8.动态内存释放
ptrsq queue_free(ptrsq sq);


