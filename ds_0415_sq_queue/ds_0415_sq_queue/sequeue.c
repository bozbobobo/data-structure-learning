//
#include "sequeue.h"


//1. 创建队列 
/***
****@return      sq:队列指针
****@para        size:用户指定的队列最大容量 
***/
ptrsq queue_init(int size)
{
	//给结构体申请空间
	ptrsq sq = (ptrsq)malloc(sizeof(sequeue));
	if (sq == NULL)
	{
		perror("queue_init: malloc failed");
		return NULL;
	}

	//给数据申请空间：
	sq->data = (data_t*)malloc(size * sizeof(data_t));
	if (sq->data == NULL)
	{
		perror("queue_init: data malloc failed");
		free(sq);
		sq = NULL;
		return NULL;
	}
	//初始化
	memset(sq->data, 0, size * sizeof(data_t));
	sq->front = sq->rear = 0;
	sq->maxsize = size;
	return sq;
}

//2. 入队
/***
**** @return      0:success     -1:failed
**** @para        sq:队列指针    val:data_t类型数据
***/
int enqueue(ptrsq sq , data_t val)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return -1;
	}
	//再判断队列满没满
	if ((sq->rear + 1) % sq->maxsize == sq->front)
	{
		printf("\nqueue is full\n");
		return -1;
	}
	//开始从队尾进入队列
	sq->data[sq->rear] = val;//先给队尾当前所在位置放上元素
	
	//注意判断队尾走到最后了没，如果走到最后，应该让队尾从头再来
	//如果没有走到最后，就正常加一位
	//这里的实现不能用简单的rear++，可以使用取余的思想：
	sq->rear = (sq->rear + 1) % sq->maxsize;

	return 0;
}

//3. 出队
/***
**** @return      data_t tmp:出队的数据     -1:failed
**** @para        sq:队列指针  
***/
data_t dequeue(ptrsq sq)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return -1;
	}

	//再判断队列空不空
	if (sq->front == sq->rear)
	{
		printf("\nqueue is empty\n");
		return -1;
	}

	//从队头删除一个元素
	//删除前先记录一下：
	data_t tmp = sq->data[sq->front];
	
	//注意判断队头走到最后了没，如果走到最后，应该让队头从0再来
	//如果没有走到最后，就正常加一位
	//这里的实现不能用简单的front++，可以使用取余的思想：
	sq->front = (sq->front + 1) % sq->maxsize;

	return tmp;
}

//4. 队列遍历打印
/***
**** @return      0:success     -1:failed
**** @para        sq:队列指针    val:data_t类型数据
***/
int queue_show(ptrsq sq)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return -1;
	}

	//再判断队列空不空
	if (sq->front == sq->rear)
	{
		printf("\nqueue is empty\n");
		return -1;
	}

	//开始遍历：
	printf("\ndata in queue:");
	int i = sq->front;
	while ( i != sq->rear )
	{
		printf("%d ", sq->data[i]);
		i = (i + 1) % sq->maxsize;
	}

	return 0;
}

//5. 清空队列 
int queue_clear(ptrsq sq)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return -1;
	}
	sq->front = sq->rear = 0;
	return 0;
}


//6. 判断队列空
/***
**** @return      1:not empty      0:empty      -1:failed
**** @para        sq:队列指针 
***/
int queue_empty(ptrsq sq)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return -1;
	}
	return (sq->front == sq->rear) ? 0 : 1;
}

//7. 判断队列满
/***
**** @return      1:full      0:not full      -1:failed
**** @para        sq:队列指针   
***/
int queue_full(ptrsq sq)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return -1;
	}
	return (sq->front == (sq->rear + 1) % sq->maxsize) ? 1 : 0;
}

//8.动态内存释放
ptrsq queue_free(ptrsq sq)
{
	//先判断队列存不存在
	if (sq == NULL)
	{
		printf("\nqueue is not exist\n");
		return NULL;
	}
	free(sq->data);
	sq->data = NULL;
	free(sq);
	sq = NULL;
	return NULL;
}