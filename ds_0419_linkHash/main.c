//
//this file test function of hash table

#include "linkhash.h"

int main()
{
	hash ht = {0};
	hash* pht = &ht;
	if(hash_creat(pht))
		return -1;
	data_t arr[MAX] = {23,34,-15,38,46,-28,68,29,07,31,26};
	int i = 0;
	for(i = 0; i < MAX; i++)
	{
		hash_insert(pht, arr[i]);
	}
	int input = 0;
	while(1)
	{
		printf("input key you want to search:");
		scanf("%d",&input);
		if(input == -1)
		{
			break;
		}
		hash_search(pht, input);
	}
	return 0;
}
