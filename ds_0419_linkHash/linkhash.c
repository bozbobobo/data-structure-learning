//
//this file realize function which declare in linkhash.h

#include "linkhash.h"

//hash function:
int hash_f(data_t key)
{
	int addr = (unsigned int)key % MAX;
	return addr;
}

//creat and init hash table:
int hash_creat(hash* h)
{
	//malloc a space to manage hash table:
	h = (hash*)malloc(sizeof(hash));
	if(NULL == h)
	{
		perror("hash_creat : hashtable malloc failed");
		return -1;
	}
	//init hash table node value:


	memset(h, 1, sizeof(hash));

/*
	int i = 0;
	for(i = 0; i < MAX;i++)
	{
		(h->hashnode[i]).key = NULLKEY;
		(h->hashnode[i]).addr = i;
		(h->hashnode[i]).next = NULL;
	}
*/
	return 0;
}

//insert data to hash table:
int hash_insert(hash* h, data_t val)
{
	if(NULL == h)
	{
		printf("hash table is NULL\n");
		return -1;
	}

	linknode* p = (linknode*)malloc(sizeof(linknode));
	if(NULL == p)
	{
		printf("\nhash_insert: linknode malloc failed");
		return -1;
	}
	//creat a new linknode to package val:
	int addr = hash_f(val);
	p->key = val;
	p->addr = addr;
	p->next = NULL;
	
	//find the hashnode which has same addr with newlinknode -p
	//and then mark this hashnode with q:
	linknode* q = NULL;
	q = &(h->hashnode[addr]);
	while(q->next != NULL)
	{
		if(q->next->key > p->key)
			break;
		q = q->next;
	}
	p->next = q->next;
	q->next = p;
	return 0;
}

//search data in hash table:
int hash_search(hash* h, data_t key)
{
	
	if(NULL == h)
	{
		printf("hash table is NULL\n");
		return -1;
	}

	int addr = hash_f(key);
	linknode* p = &(h->hashnode[addr]);
	while(p->next)
	{
		p = p->next;
		if(p->key == key)
		{
			printf("%d is found!",p->key);
			printf("It's addr is %d\n",p->addr);
			return 0;
		}
	}
	if(p->next == NULL)
	{
		printf("%d is not found!\n", key);
	}
	return -1;
}
