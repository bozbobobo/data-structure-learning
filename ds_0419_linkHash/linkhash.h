//this file define a hash table which use linklist
//
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 11
#define NULLKEY -100000000

//my data type:
typedef int data_t;

typedef struct node
{
	data_t key;//data / keywords
	int addr;//linklist node  NO.
	struct node* next;//pointer to next node which save key and addr
}linknode;

typedef struct hash
{
	linknode hashnode[MAX];
}hash;

//hash function:
int hash_f(data_t key);

//creat and init hash table:
int hash_creat(hash* h);

//insert data to hash table:
int hash_insert(hash* h, data_t val);

//search data in hash table:
int hash_search(hash* h, data_t key);




