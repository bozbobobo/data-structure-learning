#pragma once
//本文件用以声明各查找函数

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int data_t;
#define N 15

//1.顺序表查找：
int sq_search(data_t arr[], data_t key);

//有序表：
//2.折半查找：
int bin_search(data_t arr[], data_t key, int len);
//3.插值查找：
int in_search(data_t arr[], data_t key, int len);
//4.斐波那契查找：
int fibonacci(int n);
int fib_search(data_t arr[], data_t key, int len);