//
//本文件用以实现各查找算法函数

#include "search.h"

//1.顺序表查找：
int sq_search(data_t arr[], data_t key)
{
	int i = N;
	while (arr[i] != key && i != -1)
	{
		i--;
	}
	if (-1 == i)
	{
		printf("\n%d not found!\n", key);
	}
	else
	{
		printf("\nfound %d  pos is :%d\n", arr[i], i);
	}
	return i;
}

//2.折半查找：
int bin_search(data_t arr[], data_t key, int len)
{
	int left = 0;
	int right = len - 1;
	int mid = 0;
	while (left <= right)
	{
		mid = (left + right) / 2;
		if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else if (arr[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			printf("%d found . pos is: %d\n", arr[mid], mid);
			return mid;
		}
	}
	printf("%d not found\n", key);
	return -1;
}


//3.插值查找：
int in_search(data_t arr[], data_t key, int len)
{
	int left = 0;
	int right = len - 1;
	int mid = 0;
	while (left <= right)
	{
		mid = left + ((key - arr[left]) / (arr[right] - arr[left])) * (right - left);
		if (arr[mid] < key)
		{
			left = mid + 1;
		}
		else if (arr[mid] > key)
		{
			right = mid - 1;
		}
		else
		{
			printf("%d found . pos is: %d\n", arr[mid], mid);
			return mid;
		}
	}
	printf("%d not found\n", key);
	return -1;
}

//4.斐波那契查找：
//产生斐波那契数列的函数：
int fibonacci(int n)
{
	if (n == 1)
	{
		return 1;
	}
	else if (n == 0)
	{
		return 0;
	}
	return fibonacci(n - 1) + fibonacci(n - 2);
}
//斐波那契查找：
int fib_search(data_t *arr, data_t key, int len)
{
	int left = 0;
	int right = len - 1;
	int mid = 0;
	int i = 0;
	int k = 0;
	while (right > fibonacci(k) - 1)
	{
		k++;
	}
	data_t* tmp = (data_t*)malloc((fibonacci(k) - 1) * sizeof(data_t));
	if (tmp == NULL)
	{
		return -1;
	}
	memcpy(tmp, arr, len * sizeof(data_t));//将目标数组的数据都复制到临时数组
	for (i = right; i < (fibonacci(k) - 1); i++)
	{
		tmp[i] = arr[right];
	}
	left = 0;
	right = fibonacci(k) - 2;
	mid = 0;
	while (left <= right)
	{
		mid = left + fibonacci(k - 1) - 1;

		if (key < tmp[mid])
		{
			right = mid - 1;
			k = k - 1;
		}
		else if(key > tmp[mid])
		{
				left = mid +1;
				k = k - 2;
		}
		else
		{
			if (mid <= len -1)
			{
				printf("\n%d found . pos is %d\n", tmp[mid], mid);
				return mid;
			}
			else if (mid > len - 1)
			{
				printf("\n%d found . pos is %d\n", tmp[right], len - 1);
				return len - 1;
			}
		}
	}
	printf("\n%d not found!\n", key);
	free(tmp);
	tmp = NULL;
	return -1;		
}



