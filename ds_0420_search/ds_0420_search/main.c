//
//本文件用以测试各查找算法函数功能：

#include "search.h"

int main()
{
	data_t arr[N] = { 0 };
	int i = 0;
	int sum = 0;
	srand((unsigned int)time(NULL));
	for (i = 0; i < N; i++)
	{
		sum += rand() % 10;
		arr[i] = sum;
	}
	printf("数组内数字是：\n");
	for (i = 0; i < N; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n请输入你想查找的数字：\n");
	int input = 0;
	int rescanf = scanf("%d", &input);

	//1.顺序表查找：
	//sq_search(arr, input);
	
	//2.折半查找：
	//bin_search(arr, input, sizeof(arr) / sizeof(arr[0]));

	//3.斐波那契查找：
	fib_search(arr, input, sizeof(arr) / sizeof(arr[0]));
	return 0;
}