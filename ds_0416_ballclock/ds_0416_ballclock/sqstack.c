//
#include "sqstack.h"


//1. ˳��ջ�ĳ�ʼ����
/*
* @return      init success:ջָ��   init failed:NULL
* @para        maxlen:ջ��Ԫ������������û�ָ��
*/
pstack sqstack_init(int maxlen)
{
	//1.��ջ���붯̬�ڴ�
	pstack s = (pstack)malloc(sizeof(sqstack));
	if (s == NULL)
	{
		printf("\nstack init failed\n");
		return NULL;
	}

	//2.��ջ�е�����data���붯̬�ڴ棺
	s->data = (data_t*)malloc(maxlen * sizeof(data_t));
	if (s->data == NULL)
	{
		printf("\nS->data malloc failed\n");
		free(s);
		s = NULL;
		return NULL;
	}

	//3.���ݳ�ʼ����
	memset(s->data, 0, maxlen * sizeof(data_t));//ջ��������0
	s->top = -1;
	s->cap = maxlen;

	return s;
}


//2. ˳��ջ��ջ������
/*
* @return      -1:failed     0:success
* @para        s:ptr to stack    val:data to push
*/
int sqstack_push(pstack s, data_t val)
{
	//1.�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}

	//2.�ж�ջ��û����
	if (s->top == s->cap - 1)
	{
		printf("\nstack is full\n");
		return -1;
	}

	//3.��ʼ�棺
	s->top++;//ջ��ָ����+1���ڳ�һ���յط�������
	s->data[s->top] = val;

	return 0;
}

//3. ˳��ջ��ջ������
/*
* @return      -1:failed     
* @para        s:ptr to stack   
*/
data_t sqstack_pop(pstack s)
{
	//1.�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}

	//2.�ж�ջ�ղ��գ�
	if (s->top == -1)
	{
		printf("\nstack is empty\n");
		return -1;
	}

	//����ջ��Ԫ�أ�
	data_t tmp = s->data[s->top];
	memset(&(s->data[s->top]), 0, sizeof(data_t));//���
	s->top--;
	return tmp;//�����Ǹ�������ջ��Ԫ��
}


//4. �ж�˳��ջ�Ƿ�գ�
int sqstack_is_empty(pstack s)
{
	//�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}

	if (-1 == s->top)
	{
		printf("\nstack is empty\n");
		return 0;
	}
	else return 1;
}

//5. �ж�˳��ջ�Ƿ�����
int sqstack_is_full(pstack s)
{
	//�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}

	if (s->cap - 1 == s->top)
	{
		printf("\nstack is full\n");
		return 1;
	}
	else return 0;
}

//6. ������ӡ˳��ջ��Ԫ�أ�
/*
* @return      void 
* @para        s:ptr to stack   
*/
void sqstack_show(pstack s)
{
	//�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return ;
	}

	//�ж�ջ�ղ��գ�
	if (-1 == s->top)
	{
		printf("\nstack is empty\n");
		return;
	}

	for (int i = 0; i <= s->top; i++)
	{
		printf("%d ", s->data[i]);
	}

	return;
}

//7. ���˳��ջ��Ԫ�أ�
int sqstack_clear(pstack s)
{
	//�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}
	//�ж�ջ�ղ��գ�
	if (-1 == s->top)
	{
		printf("\nstack is empty\n");
		return -1;
	}
	memset(s->data, 0, (s->top + 1) * sizeof(data_t));
	s->top = -1;
	return 0;
}


//8. ����˳��ջԪ�ظ�����
int sqstack_len(pstack s)
{
	//�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}

	return s->top + 1;
}

//9. ��̬�ڴ��ͷţ�
int sqstack_free(pstack s)
{
	//�ж�ջ�治���ڣ�
	if (s == NULL)
	{
		printf("\nstack is not exist\n");
		return -1;
	}

	if (s->data != NULL)
	{
		free(s->data);
		s->data = NULL;
	}
	free(s);
	s = NULL;
	return 0;
}