#ifndef _BALLCLOCK_H_
#define _BALLCLOCK_H_
//
//
#include "linkqueue.h"
#include "sqstack.h"

int ballclock_run()
{
	int min = 0;

	//创建队列：
	linkqueue* lqueue = queue_init();
	//创建栈：
	pstack s_hour = sqstack_init(11);//小时
	pstack s_fivem = sqstack_init(11);//5分钟
	pstack s_min = sqstack_init(4);//1分钟

	//初始化队列
	for (int i = 1; i < 28; i++)
	{
		queue_enter(lqueue, i);
	}
	queue_show(lqueue);

	//**********
#if 0
	while (queue_empty(lqueue))
	{
		queue_delete(lqueue);
	}
	queue_show(lqueue);
#endif
	//**********

	if (lqueue == NULL)
	{
		printf("\nlqueue init failed\n");
		return -1;
	}

	//开始执行流程了：
	while (1)
	{
		min++;//记录时间，每循环一次是一分钟
		
		//先出队一个准备放到1分钟指示器：
		int val = queue_delete(lqueue);
		//如果1分钟栈没满就入栈：
		if (!sqstack_is_full(s_min))
		{
			sqstack_push(s_min, val);
		}
		//如果1分钟栈满了就出栈入队：
		else
		{
			while (sqstack_is_empty(s_min))
			{
				queue_enter(lqueue, sqstack_pop(s_min));
			}
			
			//准备把第5个球放到5分钟指示器：
			//如果5分钟没满就入栈：
			if (!sqstack_is_full(s_fivem))
			{
				sqstack_push(s_fivem, val);
			}

			//如果5分钟栈满了就出栈入队：
			else
			{
				while (sqstack_is_empty(s_fivem))
				{
					queue_enter(lqueue, sqstack_pop(s_fivem));
				}

				//准备将第12个球放到小时指示器：
				//如果小时栈没满就入栈：
				if (!sqstack_is_full(s_hour))
				{
					sqstack_push(s_hour, val);
				}
				//如果小时栈满了就出栈入队：
				else
				{
					while (sqstack_is_empty(s_hour))
					{
						queue_enter(lqueue, sqstack_pop(s_hour));
					}

					//准备将第12个球入队：
					queue_enter(lqueue, val);

					//检查队里的球是不是升序排列：
					//调用检查函数：如果升序，该函数返回1。其他返回0
					if (check(lqueue))
					{
						break;
					}
				}
			}
		}
	}

	return min;
}

//检查队列是不是升序
int check(linkqueue* lq)
{
	//判断队列在不在：
	if (lq == NULL)
	{
		printf("\nqueue is NULL\n");
		return 0;
	}

	//开始每两个进行检查
	ptrnode p = lq->front->next;
	while (p->next)
	{
		//如果前一个大于后一个说明不是升序，直接返回0
		if (p->data > p->next->data)
		{
			return 0;
		}
		//p后移一位
		p = p->next;
	}
	//判断完了，都是升序：
	return 1;
}


//
#endif  //  _BALLCLOCK_H_