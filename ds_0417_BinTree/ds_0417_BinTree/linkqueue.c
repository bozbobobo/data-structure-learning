//
//
#include "linkqueue.h"

//1. 创建并初始化队列
/***
**** @return     队列指针
**** @para       no
****/
linkqueue* queue_init()
{
	//申请内存管理队列结构体：
	linkqueue* lq = (linkqueue*)malloc(sizeof(linkqueue));
	if (lq == NULL)
	{
		perror("\nqueue malloc failed");
		return NULL;
	}

	//申请内存来管理结点：
	ptrnode p = (ptrnode)malloc(sizeof(node));
	if (p == NULL)
	{
		perror("\nnode malloc failed");
		free(lq);
		lq = NULL;
		return NULL;
	}

	//数据初始化：
	//刚开始队列头和队列尾相等，都指向头结点p
	p->data = 0;
	p->next = NULL;
	lq->front = lq->rear = p;
	return lq;
}

//2. 入队
/***
**** @return     0: success    -1:failed
**** @para       lq:  pointer to link queue     val:  data  
****/
int queue_enter(linkqueue* lq , data_t val)
{
	//判断队列存不存在：
	if (lq == NULL)
	{
		printf("\nqueue is NULL\n");
		return -1;
	}

	//新建一个结点：
	ptrnode p = (ptrnode)malloc(sizeof(node));
	if (p == NULL)
	{
		printf("queue_enter : new node malloc failed\n");
		return -1;
	}
	p->data = val;
	p->next = NULL;

	//将新结点p链接在上一个结点之后
	lq->rear->next = p;//链接结点
	lq->rear = p;//后移队尾
	return 0;
}

//3. 出队
/***
**** @return     -1:failed    other:data deleted
**** @para       lq:  pointer to link queue     
****/
data_t queue_delete(linkqueue* lq)
{
	//判断队列存不存在:
	if (lq == NULL)
	{
		printf("\nqueue is NULL\n");
		return -1;
	}
	//判断队列空不空:
	if (lq->front == lq->rear)
	{
		printf("\nqueue is empty\n");
		return -1;
	}

	ptrnode p = lq->front;
	lq->front = p->next;
	free(p);
	p = NULL;
	return lq->front->data;
}

//4. 遍历打印链式队列中的元素
/***
**** @return     void
**** @para       lq:  pointer to link queue  
****/
void queue_show(linkqueue* lq)
{
	//判断队列存不存在：
	if (lq == NULL)
	{
		printf("\nqueue is NULL\n");
		return ;
	}
	//判断队列空不空
	if (lq->front == lq->rear)
	{
		printf("\nqueue is empty\n");
		return;
	}

	ptrnode p = lq->front->next;
	while (p)
	{
		p = p->next;
	}
	return;
}

//5. 判断队空
/***
**** @return     0:empty    1:not empty
**** @para       lq:  pointer to link queue
****/
int queue_empty(linkqueue* lq)
{
	return (lq->front == lq->rear) ? 0 : 1;
}

//6. 计算队列中的数据个数
/***
**** @return     -1:error    other:count of queue
**** @para       lq:  pointer to link queue
****/
int queue_size(linkqueue* lq)
{
	int cnt = 0;
	//判断队列存不存在：
	if (lq == NULL)
	{
		printf("\nqueue is NULL\n");
		return -1;
	}
	//判断队列空不空
	if (lq->front == lq->rear)
	{
		return cnt;
	}
	ptrnode p = lq->front->next;
	while (p)
	{
		cnt++;
		p = p->next;
	}
	return cnt;
}

//7. 动态内存释放
linkqueue* queue_free(linkqueue* lq)
{
	//判断队列存不存在：
	if (lq == NULL)
	{
		printf("\nqueue is NULL\n");
		return -1;
	}
	ptrnode p = lq->front;
	while (p)
	{
		lq->front = lq->front->next;
		free(p);
		p = lq->front;
	}
	free(lq);
	lq = NULL;
	return NULL;
}