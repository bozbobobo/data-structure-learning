#pragma once

#include <stdio.h>
#include <stdlib.h>
//#include "linkqueue.h"

//二叉树中的数据，这里我们先用字符类型ABCD...
typedef char tree_data;

//二叉树中每个结点的结构类型：
typedef struct node
{
	tree_data data;
	struct node* left;
	struct node* right;
}bintree;


//1.二叉树的建立
bintree* tree_creat();

//2.先序遍历：
void preorder(bintree* t);

//3.中序遍历：
void inorder(bintree* t);

//4.后序遍历：
void postorder(bintree* t);

//5.层序遍历：
void layerorder(bintree* t);

