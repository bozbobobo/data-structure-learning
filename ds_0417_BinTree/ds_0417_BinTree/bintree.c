//
//

#include "bintree.h"
#include "linkqueue.h"

//1.二叉树的建立（前序遍历）
bintree* tree_creat()
{
	//让用户输入：
	tree_data ch;
	int retscanf = scanf("%c", &ch);
	if ('#' == ch)
	{
		return NULL;//递归出口：检测到#就退出这个函数，返回到上一个函数中
	}

	//申请一个结点：
	bintree* t = (bintree*)malloc(sizeof(bintree));
	if (t == NULL)
	{
		return NULL;//判断申请空间是否成功
	}

	t->data = ch;//把当前数据存入结点data域

	t->left = tree_creat();//递归：让该结点的左孩子进入递归
	t->right = tree_creat();//递归：让该结点的右孩子进入递归
	
	return t;//返回整棵二叉树的根结点
}

//2.先序遍历：
void preorder(bintree* t)
{
	if (t == NULL)
	{
		return;//递归出口
	}
	printf("%c ", t->data);//显示结点数据，可以根据需要改为其他对结点的操作
	preorder(t->left);//去遍历左子树，形成递归
	preorder(t->right);//去遍历右子树，形成递归
}

//3.中序遍历：
void inorder(bintree* t)
{
	if (NULL == t)
	{
		return;//递归出口
	}
	inorder(t->left); //去遍历左子树，形成递归
	printf("%c ", t->data);
	inorder(t->right);//去遍历右子树，形成递归
}

//4.后序遍历：
void postorder(bintree* t)
{
	if (NULL == t)
	{
		return;//递归出口
	}
	postorder(t->left);//去遍历左子树，形成递归
	postorder(t->right);//去遍历右子树，形成递归
	printf("%c ", t->data);
}

//5.层序遍历：
void layerorder(bintree* t)
{
	if (NULL == t)
	{
		return;
	}
	//创建队列
	linkqueue* queue = queue_init();
	if (queue == NULL)
	{
		return;
	}
	//访问根结点，并让根结点入队
	printf("%c ", t->data);
	queue_enter(queue, t);

	//开始循环：
	while (queue_empty(queue))
	{
		//出队一个结点：
		t = queue_delete(queue);
		//判断左右子树情况：
		if (t->left != NULL)
		{
			printf("%c ", t->left->data);
			queue_enter(queue, t->left);
		}
		if (t->right != NULL)
		{
			printf("%c ", t->right->data);
			queue_enter(queue, t->right);
		}

	}
}