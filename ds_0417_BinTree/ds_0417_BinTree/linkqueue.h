#pragma once
//
//
#include <stdio.h>
#include <stdlib.h>
#include "bintree.h"

typedef bintree* data_t;


//链表中的结点类型定义
typedef struct Node
{
	data_t data;//一个结点的数据域
	struct Node* next;//一个结点的指针域
}node, * ptrnode;

//队列的定义，这个结构体中存储了指向队列头和队列尾的结点指针
typedef struct linkqueue
{
	ptrnode front;//指向队列头结点的结点指针
	ptrnode rear;//指向队列尾结点的结点指针
}linkqueue;

//1. 创建并初始化队列
linkqueue* queue_init();

//2. 入队
int queue_enter(linkqueue* lq, data_t val);

//3. 出队
data_t queue_delete(linkqueue* lq);

//4. 遍历打印链式队列中的元素
void queue_show(linkqueue* lq);

//5. 判断队空
int queue_empty(linkqueue* lq);

//6. 计算队列中的数据个数
int queue_size(linkqueue* lq);

//7. 动态内存释放
linkqueue* queue_free(linkqueue* lq);