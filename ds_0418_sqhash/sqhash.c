//
//
#include "sqhash.h"


//function of hash:
int hash(data_t key)
{
    int fkey = (unsigned int)key % MAXSIZE;
	return fkey;
}

//creat and init a hash table:
int hash_init(hashTable* h)
{
	data_t* t  = (data_t*)malloc(MAXSIZE * sizeof(data_t));
	if(NULL == t)
	{
		printf("hash init : data malloc failed\n");
		return -1;
	}

	int i = 0;
	h->data = t;
	for(i = 0; i < MAXSIZE; i++)
	{
		h->data[i] = NULLKEY;
	}
	return 0;
}

//insert val into hash table
int hash_insert(hashTable* h , data_t key)
{
	int d = 1;
	int addr = hash(key);
	while(h->data[addr] != NULLKEY)
	{
		addr = (hash(key) + d) % MAXSIZE;
		d++;
	}
	h->data[addr] = key;
	return 0;
}


int hash_search(hashTable* h , data_t key)
{
	int addr = hash(key);
	int d = 1;
	while(h->data[addr] != key)
	{
		addr = (hash(key) +d) % MAXSIZE;
		d++;
		if(addr == hash(key))
		{
			printf("\nnot found!\n");
			return -1;
		}
	}
	printf("\nfound!%d's addr is %d",h->data[addr] ,addr);

	return addr;
}






