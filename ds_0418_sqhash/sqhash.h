//
//
#ifndef _SQHASH_H_
#define _SQHAH_H_


#include <stdio.h>
#include <stdlib.h>

#define MAXSIZE 11
#define NULLKEY -100000


typedef int data_t;

typedef struct
{
	data_t* data;
	int tableLen;
}hashTable;

int hash(data_t key);
int hash_init(hashTable* h);
int hash_insert(hashTable* h , data_t key);
int hash_search(hashTable* h , data_t key);


#endif
