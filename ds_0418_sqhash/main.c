//
//
#include "sqhash.h"

int main()
{
	hashTable hash = {0};
	data_t arr[MAXSIZE] = {23,34,14,38,46,16,68,15,07,31,26};
	if(hash_init(&hash))
	{
		return -1;
	}
	int i = 0;
	for(i = 0; i < MAXSIZE; i++)
	{
		hash_insert(&hash, arr[i]);
	}
	
	printf("\ndata in hash table:\n");
	for(i = 0; i < MAXSIZE; i++)
	{
		printf("%d ", hash.data[i]);
	}
	int input = 0;
	while(1)
	{
		printf("\nthe key you want to search:");
		scanf("%d",&input);
		if(-1 == input)
			break;
		hash_search(&hash, input);

	}
	return 0;
}
