#pragma once

//
//本文件内定义单链表结点类型结构体和各功能函数的声明

#include <stdio.h>
#include <stdlib.h>

#define HEAD_DATA 0

typedef int data_t;

//定义一个结构体类型，来表示我们一个结点的模型
//其中应该包括  数据域：data   指针域：next
typedef struct node
{
	data_t data;//数据域
	struct node* next;//指针域---指向另一个结点，所以类型是struct node*
					  //切不可丢掉*号，那样会使得这个结构体大小变成无穷
}listnode, * linklist;


//函数声明：
// 
//1.创建初始化一个结点：
linklist list_init(void );

//2. 从链表尾部插入新数据；
int list_tail_insert(linklist H, data_t val);

//3. 链表遍历打印；
int list_show(linklist H);

//4. 查找链表：按结点序号查找；
int list_search_num(linklist H, int pos);

//5. 查找链表：按数据内容查找；
int list_search_val(linklist H, data_t val);

//6. 在链表任意位置插入一个新数据；
int list_insert(linklist H, data_t val, int pos);

//7. 删除链表：按序号；
int list_delete_num(linklist H, int num);


//8. 删除链表：按内容；
int list_delete_val(linklist H, data_t val);

//9. 释放链表动态内存；
int list_free(linklist *H);

//10.求链表数据个数：
int list_len(linklist H);

//11.单向链表的反转/倒置---1 2 3 4  ->  4 3 2 1
int list_invert(linklist H);

//12.寻找相邻结点和值最大的前一个结点地址
linklist list_adj_max(linklist H);

//13.合并有序链表：
linklist list_merge(linklist H1, linklist H2);

//14.单向链表排序--冒泡排序
int list_sort(linklist H);

//15.修改链表数据：根据内容查找并修改
int list_modify(linklist H);
