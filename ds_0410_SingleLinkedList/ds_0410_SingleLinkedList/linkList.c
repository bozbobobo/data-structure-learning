#include "linkList.h"
//
//本文件内：单链表各操作实现的函数

//1.创建初始化一个结点：头结点
linklist list_init(void)
{
	//1.申请动态内存
	linklist p = (linklist)malloc(sizeof(listnode));
	if (p == NULL)
	{
		return NULL;
	}
	//2.赋值
	p->data = HEAD_DATA;
	p->next = NULL;
	//3.将结点地址返回
	return p;
}


//2. 从链表尾部插入新数据；
/****
*****@return     -1:failed     0:success
*****@para       H：链表头结点     val:要插入的值 
****/
int list_tail_insert(linklist H, data_t val)
{
	//判断H是不是空指针
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	//1.封装新结点
	linklist p = (linklist)malloc(sizeof(listnode));
	if (p == NULL)
	{
		printf("数据插入失败\n");
		return -1;
	}
	p->data = val;//把创建的结点的数据域赋值我们要插入的数据val
	p->next = NULL;//指针域置空指针，作为新的尾结点

	//2.寻找尾结点
	linklist q = H;//为了寻找尾结点，不要把传进来的H的位置改变
	while (q->next != NULL)
	{
		//不是尾结点就往后走
		q = q->next;
	}
	//跳出循环后，q->next已经是尾结点指针域

	//3.尾部插入：
	q->next = p;

	return 0;
}


//3. 链表遍历打印；
/****
*****@return     -1:failed     0:success
*****@para       H：链表头结点   
****/
int list_show(linklist H)
{
	//照例判断是不是空指针
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	if (H->next == NULL)
	{
		printf("\n空链表\n");
		return -1;
	}

	//开始遍历
	linklist p = H;
	while (p->next != NULL)
	{
		//注意要从第0个结点开始打：
		//如果是p->data就是从头结点开始
		printf("%d ", p->next->data);
		//这里一定要加上，以达到循环：
		p = p->next;
	}
	return 0;
}

//4. 查找链表：按结点序号查找；
/****
*****@return     -1:failed     0:success
*****@para       H：链表头结点    pos:需要查找的序号
****/
int list_search_num(linklist H, int pos)
{
	//照例判断H是不是空指针：
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	//如果参数太小：
	if (pos <= -1)
	{
		printf("\n位置参数有问题\n");
		return -1;
	}

	linklist p = H;
	//开始循环：
	for (int i = 0; i <= pos; i++)
	{
		p = p->next;
		//如果发现有空指针，说明已经到链表尾结点了
		//就不要再查找了，直接退出并提示找不到了
		if (p == NULL)
		{
			printf("参数有问题，没这么多数据\n");
			return -1;
		}
	}
	printf("序号为%d的数据是：%d\n", pos, p->data);

	return 0;
}

//5. 查找链表：按数据内容查找；
/****
*****@return     -1:failed     一个整数:success，是该数据val所在结点序号
*****@para       H：链表头结点    val:需要查找的数据
****/
int list_search_val(linklist H, data_t val)
{
	//照例判断H是不是空指针：
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	//开始遍历找：
	linklist p = H;
	int i = 0;//记录结点位置
	while (p->next != NULL)
	{
		if (p->next->data == val)
		{
			printf("%d的位置是第%d个结点\n", val, i);
			return i;
		}
		i++;
		p = p->next;
	}
	printf("没找到，没这个数\n");
	return -1;
}

//6. 在链表任意位置插入一个新数据；
/****
*****@return     -1:failed     0:success
*****@para       H：链表头结点    val:要插入的数据     pos:要插入的位置（结点序号）
****/
int list_insert(linklist H, data_t val, int pos)
{
	//照例判断H是不是空指针：
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	//1.申请一个新结点存放我们的数据：
	linklist p = (linklist)malloc(sizeof(listnode));
	//判断是不是空指针
	if (p == NULL)
	{
		perror("list_insert  malloc");
		return -1;
	}
	//赋值
	p->data = val;
	p->next = NULL;

	//2.寻找pos前一个结点
	int i = 0;
	linklist q = H;
	while (i <= pos)
	{
		q = q->next;
		i++;

		//特殊情况---如果走到尾结点还没到指定位置：
		if (q->next == NULL)
		{
			//直接在尾结点后追加，追加后退出函数
			printf("\n指定位置不存在，直接在链表最后追加\n");
			q->next = p;
			return 0;
		}
	}
	
	//找到之后开始链接：
	//3.链接右边箭头：
	p->next = q->next;
	//4.链接左边箭头：
	q->next = p;

	return 0;

}


//7. 删除链表：按序号；
/****
*****@return     -1:failed     0:success
*****@para       H：链表头结点    num:要删除的位置（结点序号）
****/
int list_delete_num(linklist H, int num)
{
	//照例判断H是不是空指针：
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	//num序号太小：
	if (num <= -1)
	{
		printf("\n位置参数有问题\n");
		return -1;
	}

	//1.开始定位查找要删除的num号结点的前一个结点p：
	linklist p = H;
	int i = 0;
	while (i < num)
	{
		p = p->next;
		i++;
		//遍寻到尾结点还没找到，就直接退出函数并提示：
		if (p->next == NULL)
		{
			printf("\n删除序号无效,没这么多元素\n");
			return -1;
		}
	}

	//2.找到前一个结点p，开始将要删除的结点的指针域内容复制到p的指针域中：
	linklist q = p->next;
	p->next = q->next;
	
	//3.清空：
	free(q);
	q = NULL;
	return 0;

}


//8. 删除链表：按内容；
/****
*****@return     -1:failed     0:success
*****@para       H：链表头结点    val:要删除的数据
****/
int list_delete_val(linklist H, data_t val)
{
	//照例判断H是不是空指针：
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	//开始找当前数据对应的结点序号
	linklist p = H;
	int i = 0;
	while (p->next != NULL)
	{
		p = p->next;
		if (p->data == val)
		{
			break;
		}
		i++;
	}

	if (i >= list_len(H))
	{
		printf("没有这个数\n");
		return -1;
	}

	//调用按序号删除函数：
	list_delete_num(H, i);
	return 0;
}


//9. 释放链表动态内存；
/**
***@return      -1:failed     0:success
***@para        *pH:头结点指针的指针  二级指针 
**/
int list_free(linklist *pH)
{
	//照例判断H是不是空指针：
	if (*pH == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	linklist p = *pH;

	printf("\nfree:");
	while (*pH != NULL)//注意是H而非H->next
	{
		*pH = (* pH)->next;//这条语句不能放到free后面，那样的话p和H是一样的，把H释放了就无法调用
		//从头结点开始释放
		printf("%d ", p->data);//提示一下正在释放的是哪个数据
		free(p);//此时这个p还是刚进来的时候的H，但H已经变化了。
		p = NULL;
		p = *pH;
	}
	*pH = NULL;
	return 0;
}



//10.求链表数据个数：
/****
*****@return     链表数据个数
*****@para       H：链表头结点
****/
int list_len(linklist H)
{
	//照例判断H是不是空指针：
	if (H == NULL)
	{
		printf("链表头有错\n");
		return -1;
	}

	linklist p = H;
	int cnt = 0;
	while (p->next != NULL)
	{
		p = p->next;
		cnt++;
	}

	return cnt;
}


//11.单向链表的反转/倒置---1 2 3 4  ->  4 3 2 1
/**
***@return      -1:failed     0:success
***@para        H:头结点指针 
**/
int list_invert(linklist H)
{
	//照例判断链表空指针与否
	if (H == NULL)
	{
		printf("\n链表头有误\n");
		return -1;
	}

	if (H->next == NULL)
	{
		printf("\n链表为空\n");
		return 0;
	}

	else if (H->next->next == NULL)
	{
		return 0;
	}


	//开始反转：
	linklist p = H;
	linklist q = H;

	//链表一分为二：
	p = H->next->next;
	H->next->next = NULL;

	while (q != NULL)
	{
		q = p->next;
		p->next = H->next;
		H->next = p;
		p = q;
	}
	return 0;
}

//12.寻找相邻结点和值最大的前一个结点地址
/**
***@return      和值最大的两个相邻结点中的前一个结点的地址
***@para        H:头结点指针
**/
linklist list_adj_max(linklist H)
{
	//照例判断链表空指针与否
	if (H == NULL)
	{
		printf("\n链表头有误\n");
		return -1;
	}

	//链表为空：
	if (H->next == NULL)
	{
		printf("\n链表为空\n");
		return NULL;
	}

	//链表中只有1个或者2个有效结点：
	if ((H->next->next == NULL) || (H->next->next->next  == NULL))
	{
		return H->next;
	}

	//正常开始寻找：
	int max = 0;//记录和值
	linklist p = H->next;
	linklist q = H->next->next;
	linklist tmp = p;
	while (q != NULL)
	{
		if ((p->data + q->data) > max)
		{
			max = p->data + q->data;
			tmp = p;
		}
		p = p->next;
		q = q->next;
	}
	printf("\ntmp:%p ", tmp);
	printf("手动打的最大：%p ", H->next->next->next);
	return tmp;
}


//13.合并有序链表：
/**
***@return      linklist:合并后新链表的表头
***@para        H1:链表1头结点指针    H2:链表2头结点指针
**/
linklist list_merge(linklist H1, linklist H2)
{
	//照例判断链表空指针与否
	if (H1 == NULL)
	{
		perror("list_merge H1");
		return NULL;
	}
	else if (H2 == NULL)
	{
		perror("list_merge H2");
		return NULL;
	}
	//找两个记录头结点后的第一个数据：
	linklist p = H1->next;
	linklist q = H2->next;
	//两个头结点和后面的结点断开，即指针域置空：
	H1->next = NULL;
	H2->next = NULL;
	//让H1当新的头结点，同时让tmp记录新表头：
	linklist tmp = H1;

	//当p，q没有到尾结点之前一直循环：
	while (p != NULL && q != NULL)
	{
		//比大小来确定把谁放到新表头后面：
		if (p->data <= q->data)
		{
			//p小，放p，q不动
			tmp->next = p;
			p = p->next;
			tmp = tmp->next;
			tmp->next = NULL;
		}
		else
		{
			//q小，放q，p不动
			tmp->next = q;
			q = q->next;
			tmp = tmp->next;
			tmp->next = NULL;
		}
	}

	//两个链表中有一个走到头了，那么把还没走到头的那个直接接到后面就行了
	//因为两个链表都是有序的：
	if (p == NULL)
	{
		//p先走到头了，把q接上
		tmp->next = q;
	}
	else if(q == NULL)
	{
		//q先走到头了，把p接上
		tmp->next = p;
	}

	return H1;//返回新链表表头
}

//14.单向链表排序--冒泡排序
/**
***@return      0:success    -1:failed
***@para        H:要排序的链表头结点指针
**/
int list_sort(linklist H)
{
	//照例判断链表空指针与否
	if (H == NULL)
	{
		perror("list_sort  Head");
		return NULL;
	}

	//如果链表只有头结点，即链表为空：
	if (H->next == NULL)
	{
		printf("\n链表为空\n");
		return -1;
	}

	//如果链表只有一个有效结点：
	if (H->next->next == NULL)
	{
		printf("\n链表只有一个元素，无需排序\n");
		return -1;
	}

	//一般情况：开始冒泡排序
	linklist p = H->next;//标记第一个有数据的结点：第0个结点
	linklist q = p->next;//标记第1个结点
	listnode tmp = { 0 };//定义一个中间变量，等会用来交换

	while (p->next != NULL)
	{
		while (q != NULL)
		{
			if (p->data > q->data)
			{
				tmp.data = q->data;
				q->data = p->data;
				p->data = tmp.data;
			}
			q = q->next;
		}
		p = p->next;
		q = p->next;
	}
	return 0;

}

//15.修改链表数据：根据内容查找并修改
/**
***@return      0:success    -1:failed
***@para        H:要排序的链表头结点指针
**/
int list_modify(linklist H)
{
	//照例判断链表空指针与否
	if (H == NULL)
	{
		perror("list_sort  Head");
		return NULL;
	}
	data_t val;
	printf("\n请输入您想修改的数据\n");
	int scanf_ret = scanf("%d", &val);
	linklist p = H->next;
	while (p != NULL)
	{
		if (p->data == val)
		{
			break;
		}
		p = p->next;
	}
	if (p == NULL)
	{
		printf("\n您输入的这个数据它不存在啊\n");
		return -1;
	}
	printf("\n请输入您修改后的数据：\n");
	scanf_ret = scanf("%d", &val);
	p->data = val;
	return 0;
}
