#pragma once
//
//���ļ���Ҫ����˳��ջ�Ľṹ��������������
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef int data_t;

typedef struct
{
	data_t* data;//ջ������
	int top;//ջ��λ���±꣬ջ��ָ��
	int cap;//ջ��������Ԫ������
}sqstack, * pstack;

//1. ˳��ջ�ĳ�ʼ����
pstack sqstack_init(int maxlen);

//2. ˳��ջ��ջ������
int sqstack_push(pstack s, data_t val);

//3. ˳��ջ��ջ������
data_t sqstack_pop(pstack s);

//4. �ж�˳��ջ�Ƿ�գ�
int sqstack_is_empty(pstack s);

//5. �ж�˳��ջ�Ƿ�����
int sqstack_is_full(pstack s);

//6. ������ӡ˳��ջ��Ԫ�أ�
void sqstack_show(pstack s);

//7. ���˳��ջ��Ԫ�أ�
int sqstack_clear(pstack s);

//8. ����˳��ջԪ�ظ�����
int sqstack_len(pstack s);

//9. ��̬�ڴ��ͷţ�
int sqstack_free(pstack s);