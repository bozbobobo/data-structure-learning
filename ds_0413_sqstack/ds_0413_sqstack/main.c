#include "sqstack.h"

int main()
{
	sqstack * s = sqstack_init(100);
	int val = 0;
	while (1)
	{
		printf("input:");
		int retscanf = scanf("%d", &val);
		if (val == -1) break;
		sqstack_push(s, val);
	}
	sqstack_show(s);
	printf("\nlen = %d\n", sqstack_len(s));
	printf("\n****************\n");
	for (int i = 0; i <= 5; i++)
	{
		printf("%d ", sqstack_pop(s));
	}
	printf("\n****************\n");
	sqstack_show(s);
	sqstack_clear(s);
	printf("\n****************\n");
	sqstack_show(s);

	sqstack_free(s);
	return 0;
}