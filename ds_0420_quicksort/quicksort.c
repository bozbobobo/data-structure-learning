//
// function of quicksort

#include <stdio.h>
#include <stdlib.h>

#define N 20

int q_sort(int arr[], int low, int high);
int partition(int arr[], int low, int high);
int swap(int arr[], int low, int high);

int main()
{
	int arr[N] = { 0 };
	srand((unsigned int)time(NULL));
	int i = 0;
	for(i = 0;i < N;i++)
	{
		arr[i] = rand() % 100;
	}
	printf("before sort:");
	for(i = 0;i < N;i++)
	{
		printf("%d ",arr[i]);
	}
	
	//quick sort function:
	int low = 0;
	int high = sizeof(arr) / sizeof(arr[0]) - 1;
	q_sort(arr, low, high);

	printf("\n after sort:");
	for(i = 0;i < N;i++)
	{
		printf("%d ",arr[i]);
	}
	puts("");
	return 0;
}

int q_sort(int arr[], int low, int high)
{
	if(NULL == arr)
	{
		return -1;
	}
	if(low >= high)
	{
		return 0;
	}

	//the pivot:
	int pivot = partition(arr, low, high);
	//sort left:
	q_sort(arr, low, pivot - 1);
	//sort right:
	q_sort(arr, pivot + 1, high);

}

int partition(int arr[], int low, int high)
{
	int pivot_val = arr[low];
	while(low < high)
	{
		while(low < high && (arr[high] >= pivot_val))
		{
			high--;
		}
		swap(arr, low, high);

		while(low < high && (arr[low] <= pivot_val))
		{
			low++;
		}
		swap(arr, low, high);
	}

	return low;
}

int swap(int arr[], int low, int high)
{
	int tmp = arr[high];
	arr[high] = arr[low];
	arr[low] = tmp;
	return 0;
}
