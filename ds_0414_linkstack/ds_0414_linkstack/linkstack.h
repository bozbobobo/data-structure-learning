#pragma once


//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef int data_t;

typedef struct stackNode
{
	data_t data;
	struct stackNode* next;
}stackNode, * linkStack;


//1. 链式栈的初始化；
linkStack stack_init();

//2. 链式栈入栈操作；
int stack_push(linkStack *s, data_t val);

//3. 链式栈出栈操作；
data_t stack_pop(linkStack *s);

//4. 判断链式栈是否空；
int stack_empty(linkStack *s);

//5. 遍历打印链式栈中元素；
void stack_show(linkStack *s);

//6. 计算链式栈元素个数；
int stack_len(linkStack *s);

//7. 动态内存释放；
int stack_free(linkStack *s);