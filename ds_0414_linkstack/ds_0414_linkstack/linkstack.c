
#include "linkstack.h"


//1. 链式栈的初始化；
linkStack stack_init()
{
	linkStack s = NULL;
	return s;
}

//2. 链式栈入栈操作；
int stack_push(linkStack *s, data_t val)
{

	//先判断栈空不空,如果空的，先创建第一个结点：
	if (*s == NULL)
	{
		*s = (linkStack)malloc(sizeof(stackNode));
		if (*s == NULL)
		{
			return -1;
		}
		(*s)->data = val;
		(*s)->next = NULL;
		return 0;
	}
	//不空就在链表头前面加上一个结点
	linkStack p = (linkStack)malloc(sizeof(stackNode));
	if (p == NULL)
	{
		return -1;
	}
	p->next = *s;
	p->data = val;
	*s = p;
	return 0;
}

//3. 链式栈出栈操作；
data_t stack_pop(linkStack *s)
{
	//先判断栈是不是空栈（即栈顶指针存不存在）：
	if (*s == NULL) {
		printf("\nstack is empty");
		return -1;
	}

	//如果不是空的，就把*s对应的那个结点的数据传出去
	//接着把*s这个结点给free掉
	//需要通过tmp把(*s)->next赋值*s
	data_t ret = (*s)->data;
	linkStack tmp = (*s)->next;
	free(*s);
	*s = tmp;
	return ret;
}

//4. 判断链式栈是否空；
int stack_empty(linkStack *s)
{
	//只需要判断栈顶指针是不是空
	if (*s == NULL)
	{
		printf("\nstack is empty");
		return 0;
	}
	else
	{
		printf("\nstack has data\n");
		return -1;
	}
}

//5. 遍历打印链式栈中元素；
void stack_show(linkStack *s)
{
	//先判断栈空不空：
	if (*s == NULL)
	{
		printf("\nstack error\n");
		return;
	}

	//不空，开始循环遍历打印：
	linkStack p = *s;
	while (p != NULL)
	{
		printf("%d ", p->data);
		p = p->next;
	}
	return;
}

//6. 计算链式栈元素个数；
int stack_len(linkStack *s)
{
	//先判断栈空不空：
	if (*s == NULL)
	{
		printf("\nstack is empty\n");
		return -1;
	}
	int cnt = 0;
	linkStack p = *s;
	while (p != NULL)
	{
		cnt++;
		p = p->next;
	}
	printf("\nlen = %d\n", cnt);
	return cnt;
}


//7. 动态内存释放；
int stack_free(linkStack *s)
{
	if (*s == NULL)
	{
		printf("\nstack is empty\n");
		return -1;
	}
	linkStack p = (*s);
	while ((*s) != NULL)
	{
		p = (*s);
		*s = (*s)->next;
		printf("\nfree:%d", p->data);//为了观察释放的是哪一个元素的结点，释放成功了没
		free(p);
	}
	return 0;
}