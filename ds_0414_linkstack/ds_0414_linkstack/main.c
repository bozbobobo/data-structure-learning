//
//
#include "linkstack.h"

int main()
{
	linkStack pstack = stack_init();

	while (1)
	{
		data_t val = 0;
		printf("input:");
		int ret = scanf("%d", &val);
		if (val == -1)
		{
			break;
		}
		stack_push(&pstack, val);
	}

	
	stack_show(&pstack);
	printf("\npop:%d \n",stack_pop(&pstack));
	stack_show(&pstack);
	stack_len(&pstack);
	stack_free(&pstack);
	return 0;
}