//
//
#include "linkqueue.h"


int main()
{
	int input = 0;

	//初始化函数测试：
	linkqueue* lqueue = queue_init();

	//利用循环向队列中存入数据，队列入队函数测试：
	//当输入-1就停止，也就是说输入-1前的那个数字将是队列的最后一个数据
	while (1)
	{
		printf("input:");
		int scanfret = scanf("%d", &input);
		if (input == -1)
			break;
		queue_enter(lqueue, input);
	}

	//打印队列看一下存的和我们输入的一样不一样，，队列遍历打印函数测试：
	queue_show(lqueue);

	//计算队列中数据个数，队列计数函数测试：
	printf("data count: %d ", queue_size(lqueue));

	//删除两个队列中的数，出队函数测试：
	printf("\ndata deleted: %d ", queue_delete(lqueue));
	printf("\ndata deleted: %d ", queue_delete(lqueue));

	//再次打印，看看出队两个数据后的队列：
	queue_show(lqueue);

	//释放内存，可以观察释放的内容：
	lqueue = queue_free(lqueue);
	return 0;
}